package sfedu.computergraphics.lab2;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private static final double EPS = 1e-8;

    @FXML
    private ImageView bresenhamCanvas, simpleCanvas;

    @FXML
    private Canvas bresenhamCartesianCanvas, simpleCartesianCanvas;

    @FXML
    private Label bresenhamTimeSpent, simpleTimeSpent, octantWarning;

    private Timeline octantWarningAnimation;

    private long drawCount, bresenhamTime, simpleTime;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        drawCount = 0;
        drawCartesianCoordinates(bresenhamCartesianCanvas);
        drawCartesianCoordinates(simpleCartesianCanvas);

        bresenhamCanvas.setOnMouseClicked(event -> onMouseClick(event.getX(), event.getY()));
        bresenhamCanvas.setOnMouseDragged(event -> onMouseClick(event.getX(), event.getY()));
        simpleCanvas.setOnMouseClicked(event -> onMouseClick(event.getX(), event.getY()));
        simpleCanvas.setOnMouseDragged(event -> onMouseClick(event.getX(), event.getY()));

        final int width = (int) (bresenhamCanvas.getFitWidth() + 0.5);
        final int height = (int) (bresenhamCanvas.getFitHeight() + 0.5);
        final Random random = new Random();
        final double randomX = random.nextInt(width / 2);
        final double randomY = random.nextInt((int) Math.max(1.0, randomX / width * height));
        onMouseClick(randomX, height - randomY);

        startWarningEffect();
    }

    /**
     * Stops all animations before stopping application.
     */
    void onStop() {
        octantWarningAnimation.stop();
    }

    /**
     * Add blinking effect for "only 6th octant" warning.
     */
    private void startWarningEffect() {
        octantWarningAnimation = new Timeline();
        octantWarningAnimation.setCycleCount(Animation.INDEFINITE);
        octantWarningAnimation.getKeyFrames().add(
                new KeyFrame(Duration.seconds(0.6),
                        (e) -> {
                            if (octantWarning.getTextFill() == Color.RED) {
                                octantWarning.setTextFill(Color.DARKRED);
                            } else if (octantWarning.getTextFill() == Color.DARKRED) {
                                octantWarning.setTextFill(Color.RED);
                            }
                        }
                )
        );
        octantWarningAnimation.play();
    }

    /**
     * Mouse click event listener: if specified point belongs to 6-th octant,
     * draw lines from center to it on both images.
     * Otherwise warning user about his mistake.
     *
     * @param imageX X coordinate (in image coordinates) to draw line to.
     * @param imageY Y coordinate (in image coordinates) to draw line to.
     */
    private void onMouseClick(final double imageX, final double imageY) {
        final double imageHeight = simpleCanvas.getFitHeight();
        final double imageWidth = simpleCanvas.getFitWidth();
        final double deltaX = imageWidth / 2 - imageX;
        final double deltaY = imageY - imageHeight / 2;
        if (deltaX < 0 || deltaY < 0 || deltaY / imageHeight < deltaX / imageWidth) {
            octantWarning.setTextFill(Color.RED);
        } else {
            octantWarning.setTextFill(Color.BLACK);
        }
        drawCount++;
        drawBresenhamsImage(imageWidth / 2, imageHeight / 2, imageX, imageY);
        drawSimpleImage(imageWidth / 2, imageHeight / 2, imageX, imageY);
    }

    /**
     * Draw circumference and its radius from (x1; y1) to (x2; y2) with Bresenham's algorithm.
     *
     * @param x1 Starting X coordinate.
     * @param y1 Starting Y coordinate.
     * @param x2 Finishing X coordinate.
     * @param y2 Finishing Y coordinate.
     */
    private void drawBresenhamsImage(final double x1, final double y1, final double x2, final double y2) {
        final int width = (int) (bresenhamCanvas.getFitWidth() + 0.5);
        final int height = (int) (bresenhamCanvas.getFitHeight() + 0.5);
        final int deltaX = (int) (x1 - x2);
        final int deltaY = (int) (y2 - y1);
        final WritableImage image = new WritableImage(width, height);
        final PixelWriter writer = image.getPixelWriter();
        long beginTime = System.nanoTime();
        drawBresenhamsLine(x1, y1, x2, y2, writer);
        drawBresenhamsCircumference(x1, y1, Math.sqrt(deltaX * deltaX + deltaY * deltaY), writer);
        long endTime = System.nanoTime();
        bresenhamTime += endTime - beginTime;
        bresenhamCanvas.setImage(image);
        bresenhamTimeSpent.setText(Double.toString(bresenhamTime / drawCount / 1000.0) + " ms");
    }

    /**
     * Draw circumference with center in (x1; y1) and radius r with Bresenham's algorithm.
     *
     * @param x1     X coordinate of center.
     * @param y1     Y coordinate of center.
     * @param r      Radius value.
     * @param writer PixelWriter to draw line on.
     */
    private void drawBresenhamsCircumference(final double x1, final double y1,
                                             final double r, final PixelWriter writer) {
        double d = 3 - 2 * r;
        for (double y = r, x = 0; x < y + EPS; x++) {
            try {
                writer.setColor((int) (x1 + x), (int) (y1 + y), Color.RED);
                writer.setColor((int) (x1 + x), (int) (y1 - y), Color.RED);
                writer.setColor((int) (x1 - x), (int) (y1 - y), Color.RED);
                writer.setColor((int) (x1 - x), (int) (y1 + y), Color.RED);
                writer.setColor((int) (x1 - y), (int) (y1 + x), Color.RED);
                writer.setColor((int) (x1 - y), (int) (y1 - x), Color.RED);
                writer.setColor((int) (x1 + y), (int) (y1 - x), Color.RED);
                writer.setColor((int) (x1 + y), (int) (y1 + x), Color.RED);
            } catch (final IndexOutOfBoundsException exc) {
            }
            if (d < 0) {
                d += 4 * x + 6;
            } else {
                d += 4 * (x - y) + 6;
                y--;
            }
        }
    }

    /**
     * Draw line down-left from (x1; y1) to (x2; y2) with Bresenham's algorithm.
     *
     * @param x1     Starting X coordinate.
     * @param y1     Starting Y coordinate.
     * @param x2     Finishing X coordinate.
     * @param y2     Finishing Y coordinate.
     * @param writer PixelWriter to draw line on.
     */
    private void drawBresenhamsLine(double x1, double y1, double x2, double y2, final PixelWriter writer) {
        final int deltaX = (int) (x1 - x2);
        final int deltaY = (int) (y2 - y1);
        final int y2i = (int) (y2 + 0.5);
        int error = 0;
        int x = (int) x1;
        for (int y = (int) y1; y <= y2i; y++) {
            try {
                writer.setColor(x, y, Color.RED);
                error += deltaX;
                if (2 * error >= deltaY) {
                    x--;
                    error -= deltaY;
                }
            } catch (final IndexOutOfBoundsException exc) {
            }
        }
    }

    /**
     * Draw circumference and its radius from (x1; y1) to (x2; y2) with simple algorithm.
     *
     * @param x1 Starting X coordinate.
     * @param y1 Starting Y coordinate.
     * @param x2 Finishing X coordinate.
     * @param y2 Finishing Y coordinate.
     */
    private void drawSimpleImage(final double x1, final double y1, final double x2, final double y2) {
        final int width = (int) (simpleCanvas.getFitWidth() + 0.5);
        final int height = (int) (simpleCanvas.getFitHeight() + 0.5);
        final int deltaX = (int) (x1 - x2);
        final int deltaY = (int) (y2 - y1);
        final WritableImage image = new WritableImage(width, height);
        final PixelWriter writer = image.getPixelWriter();
        final long beginTime = System.nanoTime();
        drawSimpleLine(x1, y1, x2, y2, writer);
        drawSimpleCircumference(x1, y1, Math.sqrt(deltaX * deltaX + deltaY * deltaY), writer);
        final long endTime = System.nanoTime();
        simpleTime += endTime - beginTime;
        simpleCanvas.setImage(image);
        simpleTimeSpent.setText(Double.toString(simpleTime / drawCount / 1000.0) + " ms");
    }

    /**
     * Draw circumference with center in (x1; y1) and radius r with simple algorithm.
     *
     * @param x1     X coordinate of center.
     * @param y1     Y coordinate of center.
     * @param r      Radius value.
     * @param writer PixelWriter to draw line on.
     */
    private void drawSimpleCircumference(final double x1, final double y1,
                                         final double r, final PixelWriter writer) {
        final double angleTarget = 1.5 * Math.PI;
        for (double angle = 1.25 * Math.PI; angle < angleTarget; angle += 0.01) {
            try {
                double dx = Math.cos(angle) * r;
                double dy = Math.sin(angle) * r;
                writer.setColor((int) (x1 + dx), (int) (y1 + dy), Color.RED);
                writer.setColor((int) (x1 - dx), (int) (y1 + dy), Color.RED);
                writer.setColor((int) (x1 - dx), (int) (y1 - dy), Color.RED);
                writer.setColor((int) (x1 + dx), (int) (y1 - dy), Color.RED);
                writer.setColor((int) (y1 + dy), (int) (x1 + dx), Color.RED);
                writer.setColor((int) (y1 - dy), (int) (x1 + dx), Color.RED);
                writer.setColor((int) (y1 - dy), (int) (x1 - dx), Color.RED);
                writer.setColor((int) (y1 + dy), (int) (x1 - dx), Color.RED);
            } catch (final IndexOutOfBoundsException exc) {
            }
        }
    }

    /**
     * Draw line down-left from (x1; y1) to (x2; y2) with simple algorithm.
     *
     * @param x1     Starting X coordinate.
     * @param y1     Starting Y coordinate.
     * @param x2     Finishing X coordinate.
     * @param y2     Finishing Y coordinate.
     * @param writer PixelWriter to draw line on.
     */
    private void drawSimpleLine(double x1, double y1, double x2, double y2, final PixelWriter writer) {
        final double dx = (x2 - x1) / (y2 - y1);
        final double y2e = y2 + EPS;
        double x = x1 + 0.5; // +0.5 instead of Math.round
        for (double y = y1; y < y2e; y++) {
            try {
                writer.setColor((int) x, (int) y, Color.RED);
                x += dx;
            } catch (final IndexOutOfBoundsException exc) {
            }
        }
    }

    /**
     * Draw cartesian coordinates on given canvas.
     *
     * @param canvas An canvas to draw on.
     */
    private void drawCartesianCoordinates(final Canvas canvas) {
        final GraphicsContext context = canvas.getGraphicsContext2D();
        final double width = canvas.getWidth();
        final double height = canvas.getHeight();
        context.setStroke(Color.LIGHTGRAY);
        context.strokeLine(width / 2, 0, width / 2, height);
        context.strokeLine(0, height / 2, width, height / 2);
        context.strokeLine(0, 0, width, height);
        context.strokeLine(width, 0, 0, height);
    }
}
